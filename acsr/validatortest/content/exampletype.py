"""Definition of the Example Type content type
"""

from zope.interface import implements

from Products.Archetypes import atapi
from Products.ATContentTypes.content import base
from Products.ATContentTypes.content import schemata

# -*- Message Factory Imported Here -*-
from acsr.validatortest import validatortestMessageFactory as _

from acsr.validatortest.interfaces import IExampleType
from acsr.validatortest.config import PROJECTNAME

ExampleTypeSchema = schemata.ATContentTypeSchema.copy() + atapi.Schema((

    # -*- Your Archetypes field definitions here ... -*-

    atapi.StringField(
        'url',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"URL"),
            description=_(u"a valid url"),
        ),
        required=True,
        validators=('isURL'),
    ),


    atapi.StringField(
        'zipcode',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"ZIP Code"),
            description=_(u"ta five digit zipcode"),
        ),
        required=True,
        validators=('isZipCode'),
    ),


    atapi.StringField(
        'mail',
        storage=atapi.AnnotationStorage(),
        widget=atapi.StringWidget(
            label=_(u"eMail"),
            description=_(u"Test mailvalidator"),
        ),
        required=True,
        validators=('isEmail'),
    ),


))

# Set storage on fields copied from ATContentTypeSchema, making sure
# they work well with the python bridge properties.

ExampleTypeSchema['title'].storage = atapi.AnnotationStorage()
ExampleTypeSchema['description'].storage = atapi.AnnotationStorage()

schemata.finalizeATCTSchema(ExampleTypeSchema, moveDiscussion=False)


class ExampleType(base.ATCTContent):
    """Description of the Example Type"""
    implements(IExampleType)

    meta_type = "ExampleType"
    schema = ExampleTypeSchema

    title = atapi.ATFieldProperty('title')
    description = atapi.ATFieldProperty('description')

    # -*- Your ATSchema to Python Property Bridges Here ... -*-
    url = atapi.ATFieldProperty('url')

    zipcode = atapi.ATFieldProperty('zipcode')

    mail = atapi.ATFieldProperty('mail')


atapi.registerType(ExampleType, PROJECTNAME)
