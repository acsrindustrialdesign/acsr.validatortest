from zope.interface import Interface
# -*- Additional Imports Here -*-
from zope import schema

from acsr.validatortest import validatortestMessageFactory as _



class IExampleType(Interface):
    """Description of the Example Type"""

    # -*- schema definition goes here -*-
    url = schema.TextLine(
        title=_(u"URL"),
        required=True,
        description=_(u"a valid url"),
    )
#
    zipcode = schema.TextLine(
        title=_(u"ZIP Code"),
        required=True,
        description=_(u"ta five digit zipcode"),
    )
#
    mail = schema.TextLine(
        title=_(u"eMail"),
        required=True,
        description=_(u"Test mailvalidator"),
    )
#
